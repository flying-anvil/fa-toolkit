# FA-Toolkit

## Installation

`composer global require flying-anvil/fa-toolkit`

Find the installation path:  
`composer global config bin-dir --absolute 2&>/dev/null`

Add it to your `PATH` environment variable in your `~/.bashrc` (or equivalent):  
`export PATH=PATH:/output/from/above`

## Usage

See the markdown files in the [documentation directory](https://gitlab.com/flying-anvil/fa-toolkit/-/tree/master/documentation).
