# Uptime Calc

Prints a message for how long you have to stay at work.
This is pretty proprietary as it is not configurable (yet).

## Usage

Example: `bin/uptime-calc`

```
==============================
= Du musst noch 3:53 bleiben =
==============================
```

## uptime-calc-tab-title

This is a bash script that put the remaining time on the terminals title.

- Example: `bin/uptime-calc-tab-title`
- Example: `bin/uptime-calc-tab-title &`


