# Unit Beautifier

## Usage

Run unit tests normally, but pipe the output to `bin/phpunit-beautifier`.  
Example: `phpdbg -qrr ./vendor/bin/phpunit -c phpunit.xml | bin/phpunit-beautifier`

## Output

Regular Output:
```
PHPUnit 8.5.2 by Sebastian Bergmann and contributors.

Runtime:       PHPDBG 7.4.2
Configuration: /app/phpunit.xml

................................................................. 65 / 76 ( 85%)
...........                                                       76 / 76 (100%)

Time: 805 ms, Memory: 26.00 MB

OK (76 tests, 358 assertions)

Generating code coverage report in HTML format ... done [374 ms]
```

Beautified Output:

```
PHPUnit 8.5.2 by Sebastian Bergmann and contributors.

Runtime:       PHPDBG 7.4.2
Configuration: /app/phpunit.xml

 ✓ Passed     ⚡ Error    ❌ Failure    ▶ Skipped    ⚠ Risky    ⚠ Warning    🚧 Incomplete    

✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓  65 / 76 ( 85%)
✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓ ✓                                                                                                              76 / 76 (100%)

Time: 831 ms, Memory: 26.00 MB

OK (76 tests, 358 assertions)

Generating code coverage report in HTML format ... done [362 ms]
```
